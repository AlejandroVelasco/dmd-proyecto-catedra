# -*- coding: utf-8 -*-
"""
Created on Thu May  6 19:46:09 2021

@author: aleev
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.cluster import KMeans

dataset = pd.read_csv("parque_vehicular_datos.csv", sep=";")
#creamos variables
x = dataset.iloc[:30000, [1,5]].values
#modelo de codo para encontrar numero optimo de grupos
# wcss = []
# for i in range(1, 11):
#     kmeans = KMeans(n_clusters = i, init = 'k-means++', random_state = 42)
#     kmeans.fit(x)
#     wcss.append(kmeans.inertia_)
# plt.plot(range(1,11), wcss)
# plt.title("Método del codo")
# plt.xlabel("Número de grupos")
# plt.ylabel("WCSS")
# plt.show()

#Entrenamiento del modelo kmeans en el conjunto de datos
kmeans = KMeans(n_clusters = 4, init = "k-means++", random_state = 42)
y_kmeans = kmeans.fit_predict(x)

#Visualizando los clusters
plt.scatter(x[y_kmeans == 0, 0], x[y_kmeans == 0, 1], s = 100, c = 'red', label = 'Cluster 1')
plt.scatter(x[y_kmeans == 1, 0], x[y_kmeans == 1, 1], s = 100, c = 'blue', label = 'Cluster 2')
plt.scatter(x[y_kmeans == 2, 0], x[y_kmeans == 2, 1], s = 100, c = 'pink', label = 'Cluster 3')
plt.scatter(x[y_kmeans == 3, 0], x[y_kmeans == 3, 1], s = 100, c = 'purple', label = 'Cluster 4')

plt.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:, 1], s = 300, c = 'yellow', label = 'Centroides')
plt.title('Clusters de parque vehicular')
plt.xlabel('Anio de fabricacion')
plt.ylabel('Valor del bvechiculo')
plt.legend()
plt.show()
